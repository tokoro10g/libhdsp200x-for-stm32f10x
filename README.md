libHDSP200x for STM32F10x
======================
A library to use HDSP200x led display in STM32.

Files
------

name            | description
----------------|----------------------------------------
bin/            | Output folder for binary files  
lib/            | StdPeriph library by STMicroElectronics  
lib2/           | Misc. libraries for board specific settings  
hdsp2000lp.c    | HDSP2000LP library C source code  
hdsp2000lp.h    | HDSP2000LP library Header  
matrix.h        | Character bit matrix  
main.c          | Example code: main routine  
stm32f10x_it.c  | Example code: interrupt handlers  
stm32f10x_it.h  | Example code: interrupt handlers header  
makefile        | Makefile  

Usage
------

First, you can modify some settings around GPIO pins.

    /* local macros */
    /* Please modify to use */
    #define HDSP_COL_GPIO GPIOA			// GPIOx is GPIOB
    #define HDSP_COL_PIN(x) 1<<(x+3)	// COLx (x=[0:4]) are assigned to PB(x+3)
    #define HDSP_CLK_GPIO GPIOB			// GPIOx is GPIOB
    #define HDSP_CLK_PIN 1<<9			// CLK is assigned to PB9
    #define HDSP_DAT_GPIO GPIOB			// GPIOx is GPIOB
    #define HDSP_DAT_PIN 1<<8			// DAT is assigned to PB8
    
Then, write something like this:

### Example code ###
    GPIO_Configuration();  // Write by yourself
    TIM_Configuration();   // Write by yourself
    HDSP_LoadStr("hoge",HDSP_ASCII);  // First, load string to be displayed
    HDSP_SetStatus(HDSP_STATUS_ENABLE); // Then, enable.
 

Description of local variables
------------------------------

    static uint8_t HDSP_Bits[5][7*4]

Data of character pixels. 7*4=28 bits per column.

----

    static uint8_t HDSP_CurrentCol

Number of a column which is currently refreshed.

----

    static uint8_t HDSP_Status

Enable/Disable status. Default is ```HDSP_STATUS_DISABLE```

Description of functions
-------------------------

    void HDSP_LoadStr(const char *str, uint8_t flag)
    
Load a string to be displayed.

+   `str` :
    string to be displayed
 
+   `flag` :
    flag to determine the type of the string
    This parameter can be one of the following values:

    name                | description
    --------------------|-------------------------------------
    ```HDSP_ASCII```	| use ascii code
    ```HDSP_NIHONGO```	| use Japanese Hiragana and Katakana

----

    void HDSP_LoadInt(uint16_t n)
    
 Load an integer to be displayed.
 
+   `n` :
    integer to be displayed

----

    void HDSP_SelectCol(uint8_t col)
    
Select a column
 
+   `col` :
    column to be selected
 
----

    void HDSP_OutputCol(uint8_t col)
    
Send data to a column
 
+   `col` :
    column to be selected

----

    void HDSP_SetStatus(uint8_t newstatus)
    
Set the enable/disable status
 
+   `newstatus` : status to be set
    This parameter can be one of the following values:

    name                        | description
    ----------------------------|--------------------
    ```HDSP_STATUS_ENABLE```	| enable displaying
    ```HDSP_STATUS_DISABLE```	| disable displaying
    
----

    void HDSP_Refresh()
    
Refresh one of the columns

This function should be called periodically by a timer.

License
----------
Copyright (c)2013 Tokoro
Distributed under the [MIT License][mit].

[MIT]: http://www.opensource.org/licenses/mit-license.php