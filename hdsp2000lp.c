/*
 * hdsp2000lp.c
 *
 *  Created on: 2012/12/04
 *      Author: Tokoro
 */

#include "hdsp2000lp.h"
#include "matrix.h"

static uint8_t HDSP_Bits[5][7 * 4] = { };
static uint8_t HDSP_CurrentCol = 0;
static uint8_t HDSP_Status = HDSP_STATUS_DISABLE;

/**
 * @brief Load a string to be displayed.
 * @param str:	string to be displayed
 * @param flag:	flag to determine the type of the string
 * 				This parameter can be one of the following values:
 * 				@arg HDSP_ASCII		use ascii code
 * 				@arg HDSP_NIHONGO	use Japanese Hiragana and Katakana
 * @retval None
 */
void HDSP_LoadStr(const char *str, uint8_t flag) {
	int i;
	for (i = 0; i < 4; i++) {
		char ch = str[i];
		ch -= 32; // 32 = 0x20 = ' ' is the first character in the table
		if (ch < 0)
			ch = 0;
		int row;
		for (row = 0; row < 7; row++) {
			char bits;
			if (flag == HDSP_NIHONGO) {
				bits = nihongo_data[(int) ch][row]; // Japanese Hiragana/Katakana
			} else {
				bits = ascii_data[(int) ch][row]; // ascii character
			}
			int col;
			for (col = 0; col < 5; col++) {
				HDSP_Bits[col][row + i * 7] = (bits & (1 << col));
			}
		}
	}
}

/**
 * @brief Load an integer to be displayed.
 * @param n:	integer value to be displayed
 * @retval None
 */
void HDSP_LoadInt(uint16_t n) {
	char str[4];
	n = n % 10000;	// limited to 0000-9999
	str[0] = n / 1000 + '0';
	str[1] = (n % 1000) / 100 + '0';
	str[2] = (n % 100) / 10 + '0';
	str[3] = n % 10 + '0';

	HDSP_LoadStr(str, HDSP_ASCII);
}

/**
 * @brief Select a column
 * @param col:	column to be selected
 * @retval None
 */
void HDSP_SelectCol(uint8_t col) {
	int i;
	for (i = 0; i < 5; i++) {
		GPIO_ResetBits(HDSP_COL_GPIO, HDSP_COL_PIN(i));
	}
	if (col > 4)
		return;
	GPIO_SetBits(HDSP_COL_GPIO, HDSP_COL_PIN(col));
}

/**
 * @brief Send data to a column
 * @param col:	column to be selected
 * @retval None
 */
void HDSP_OutputCol(uint8_t col) {
	// select a column
	HDSP_SelectCol(col);
	for (uint8_t i = 0; i < 4 * 7; i++) {
		// push data (28 bits)
		GPIO_SetBits(HDSP_CLK_GPIO, HDSP_CLK_PIN);
		if (HDSP_Bits[col][i]) {
			GPIO_SetBits(HDSP_DAT_GPIO, HDSP_DAT_PIN);
		} else {
			GPIO_ResetBits(HDSP_DAT_GPIO, HDSP_DAT_PIN);
		}
		GPIO_ResetBits(HDSP_CLK_GPIO, HDSP_CLK_PIN);
	}
}

/**
 * @brief Set the enable/disable status
 * @param newstatus:	status to be set
 * 						This parameter can be one of the following values:
 * 						@arg HDSP_STATUS_ENABLE		enable displaying
 * 						@arg HDSP_STATUS_DISABLE	disable displaying
 * @retval None
 */
void HDSP_SetStatus(uint8_t newstatus) {
	HDSP_Status = newstatus;
}

/**
 * @brief Refresh one of the columns
 * This function should be called periodically by a timer.
 * @param None
 * @retval None
 */
void HDSP_Refresh() {
	if (HDSP_Status == HDSP_STATUS_DISABLE)
		return;
	HDSP_CurrentCol++;
	if (HDSP_CurrentCol > 4)
		HDSP_CurrentCol = 0;
	HDSP_OutputCol(HDSP_CurrentCol);

	// delay to adjust brightness
	for (int i = 0; i < 600; i++)
		;
	HDSP_SelectCol(5);
}
