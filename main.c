/*
 * HDSP2000LP Library example for STM32VLDiscovery (STM32F10x)
 *
 * author Tokoro
 *
 * You can modify settings in hdsp2000lp.h
 *
 */

#include "stm32f10x.h"
#include "platform_config.h"
#include "hdsp2000lp.h"

void GPIO_Configuration(void);
void TIM_Configuration(void);

#define NUMSTR 3
const char strlist[NUMSTR][4] = {
		"xT|~", // ロギー
		" C+!", // あやしい
		"B;B;", // もふもふ
};
uint8_t strindex = 0;

int main(void) {
	/* Board specific settings */
	BoardInit();
	/* GPIO Configuration */
	GPIO_Configuration();
	/* Timer Configuration to refresh display */
	TIM_Configuration();

	/* First, load string from strlist[0] in Japanese mode. */
	HDSP_LoadStr(strlist[0], HDSP_NIHONGO);
	/* Then, enable. */
	HDSP_SetStatus(HDSP_STATUS_ENABLE);

	while (1) {
	}
	return 0;
}

void GPIO_Configuration(void) {
	//Supply APB2 Clock
	RCC_APB2PeriphClockCmd(
			RCC_APB2Periph_GPIOA | RCC_APB2Periph_GPIOB | RCC_APB2Periph_GPIOC
					| RCC_APB2Periph_AFIO, ENABLE);

	GPIO_InitTypeDef GPIO_InitStructure;

	/*********/
	/* GPIOA */
	/*********/

	/* Configure OB_SW: input floating */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	/*********/
	/* GPIOB */
	/*********/

	/* Configure HDSP2000LP: output push-pull */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_3 | GPIO_Pin_4 | GPIO_Pin_5
			| GPIO_Pin_6 | GPIO_Pin_7 | GPIO_Pin_8 | GPIO_Pin_9;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOB, &GPIO_InitStructure);

	/*********/
	/* GPIOC */
	/*********/

	/* Configure OB_LED: output push-pull */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOC, &GPIO_InitStructure);

	/* Configure EXTI for OB_SW */
	GPIO_EXTILineConfig(GPIO_PortSourceGPIOA, GPIO_PinSource0);

	EXTI_InitTypeDef EXTI_InitStruct;
	EXTI_InitStruct.EXTI_Line = EXTI_Line0;
	EXTI_InitStruct.EXTI_Mode = EXTI_Mode_Interrupt;
	EXTI_InitStruct.EXTI_Trigger = EXTI_Trigger_Rising;
	EXTI_InitStruct.EXTI_LineCmd = ENABLE;
	EXTI_Init(&EXTI_InitStruct);

	/* Configure NVIC for EXTI */
	NVIC_InitTypeDef NVIC_InitStruct;
	NVIC_InitStruct.NVIC_IRQChannel = EXTI0_IRQn;
	NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = 0x00;
	NVIC_InitStruct.NVIC_IRQChannelSubPriority = 0x00;
	NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStruct);

	HDSP_SelectCol(5); // clear
}

void TIM_Configuration() {
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM4, ENABLE);
	TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStruct;

	/* Configure time base of TIM4 */
	TIM_TimeBaseInitStruct.TIM_Period = 49;
	TIM_TimeBaseInitStruct.TIM_Prescaler = 719;
	TIM_TimeBaseInitStruct.TIM_ClockDivision = TIM_CKD_DIV4;
	TIM_TimeBaseInitStruct.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_TimeBaseInit(TIM4, &TIM_TimeBaseInitStruct);

	TIM_ITConfig(TIM4, TIM_IT_Update, ENABLE);
	TIM_Cmd(TIM4, ENABLE);

	/* Configure NVIC for TIM4_IT */
	NVIC_InitTypeDef NVIC_InitStruct;
	NVIC_InitStruct.NVIC_IRQChannel = TIM4_IRQn;
	NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = 0x03;
	NVIC_InitStruct.NVIC_IRQChannelSubPriority = 0x01;
	NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStruct);
}
