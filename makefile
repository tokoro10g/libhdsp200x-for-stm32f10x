SHELL = cmd.exe
TARGET_ARCH   = -mcpu=cortex-m3 -mthumb
INCLUDE_DIRS  = -I lib/STM32F10x_StdPeriph_Driver/inc \
				-I lib/CMSIS/CM3/DeviceSupport/ST/STM32F10x \
				-I lib/CMSIS/CM3/CoreSupport \
				-I lib2
STARTUP_DIR = lib/CMSIS/CM3/DeviceSupport/ST/STM32F10x/startup/gcc_ride7/
BOARD_OPTS = -DHSE_VALUE=((uint32_t)8000000) -DSTM32F10X_MD_VL \
             -DUSE_STM32_VLD -DSYSCLK_FREQ_24MHz=24000000
FIRMWARE_OPTS = -DUSE_STDPERIPH_DRIVER
COMPILE_OPTS  = -fsigned-char -Wall -fmessage-length=0 $(INCLUDE_DIRS) $(BOARD_OPTS) $(FIRMWARE_OPTS)

TOOLDIR = ../../yagarto/bin/
CC      = $(TOOLDIR)arm-none-eabi-gcc
AS      = $(CC)
LD      = $(CC)
AR      = $(TOOLDIR)arm-none-eabi-ar
OBJCOPY = $(TOOLDIR)arm-none-eabi-objcopy
CFLAGS  = -std=gnu99 $(COMPILE_OPTS)
ASFLAGS = -x assembler-with-cpp -c $(TARGET_ARCH) $(COMPILE_OPTS) 
LDFLAGS = -Wl,--gc-sections,-Map=bin\main.map,-cref -T lib2/128_8.ld $(INCLUDE_DIRS) -L ./lib

all: lib/lib.a bin\main.hex

# main.o is compiled by suffix rule automatucally
bin\main.hex: main.o hdsp2000lp.o lib/lib.a stm32f10x_it.o $(STARTUP_DIR)startup_stm32f10x_md_vl.o
	$(LD) $(LDFLAGS) $(TARGET_ARCH) $^ -o bin\main.elf 
	$(OBJCOPY) -O ihex bin\main.elf bin\main.hex

# many of xxx.o are compiled by suffix rule automatically
LIB_OBJS = $(sort \
 $(patsubst %.c,%.o,$(wildcard lib/*.c)) \
 $(patsubst %.c,%.o,$(wildcard lib2/*.c)) \
 $(patsubst %.c,%.o,$(wildcard lib/STM32F10x_StdPeriph_Driver/src/*.c)) \
 $(patsubst %.c,%.o,$(wildcard lib/CMSIS/CM3/DeviceSupport/ST/STM32F10x/*.c)) \
 $(patsubst %.s,%.o,$(wildcard $(STARTUP_DIR)startup_stm32f10x_md_vl.s)))

lib/lib.a: $(LIB_OBJS)
	$(AR) cr lib/lib.a $(LIB_OBJS)

$(LIB_OBJS): \
 $(wildcard lib/STM32F10x_StdPeriph_Driver/inc/*.h) \
 $(wildcard lib/STM32F10x_StdPeriph_Driver/src/*.c) \
 $(wildcard lib/CMSIS/CM3/DeviceSupport/ST/STM32F10x/*.h) \
 $(wildcard lib/CMSIS/CM3/DeviceSupport/ST/STM32F10x/*.c) \
 $(wildcard $(STARTUP_DIR)startup_stm32f10x_md_vl.s) \
 $(wildcard lib2/*.h) \
 $(wildcard lib2/*.c) \
 makefile

all_clean:
	del /f /q *.o *.s bin\* lib\*.a lib\*.o lib2\*.o
	del /f /q lib\STM32F10x_StdPeriph_Driver\src\*.o
	del /f /q lib\CMSIS\CM3\DeviceSupport\ST\STM32F10x\startup\gcc_ride7\*.o
